package com.example.adifa_1202164081_si4001_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText edt_Alas;
    private EditText edt_Tinggi;
    private TextView edt_Hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    protected void hitung(View view){
        edt_Alas = findViewById(R.id.alas_editText);
        edt_Tinggi =  findViewById(R.id.tinggi_editText);
        edt_Hasil = findViewById(R.id.hasil_textView);

        Integer alas = Integer.parseInt(edt_Alas.getText().toString());
        Integer tinggi = Integer.parseInt(edt_Tinggi.getText().toString());
        Integer hasil = alas*tinggi;
        edt_Hasil.setText(String.valueOf(hasil));
    }
}
